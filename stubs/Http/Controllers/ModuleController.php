<?php

namespace {namespace}\{Module}\Http\Controllers{SubDir};

use Illuminate\Routing\Controller;

class {Module}Controller extends Controller
{
    public function vue()
    {
        return view('{module}::vue.index');
    }

    public function blade()
    {
        return view('{module}::blade.index');
    }

}