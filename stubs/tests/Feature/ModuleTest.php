<?php

use {namespace}\{Module}\Models\{Model};

uses(Tests\TestCase::class);

test('can see {model} list', function() {
    $this->authenticate();
   $this->get(route('addon.{module}.index'))->assertOk();
});

test('can see {model} create page', function() {
    $this->authenticate();
   $this->get(route('addon.{module}.create'))->assertOk();
});

test('can create {model}', function() {
    $this->authenticate();
   $this->post(route('addon.{module}.store', [
       'name' => 'Joe'
   ]))->assertRedirect(route('addon.{module}.index'));

   $this->assertDatabaseCount('{module}', 1);
});

test('can see {model} edit page', function() {
    $this->authenticate();
    ${model} = {Model}::factory()->create();
    $this->get(route('addon.{module}.edit', ${model}->id))->assertOk();
});

test('can update {model}', function() {
    $this->authenticate();
    ${model} = {Model}::factory()->create();
    $this->patch(route('addon.{module}.update', ${model}->id), [
        'name' => 'Joe Smith'
    ])->assertRedirect(route('addon.{module}.index'));

    $this->assertDatabaseHas('{module}', ['name' => 'Joe Smith']);
});

test('can delete {model}', function() {
    $this->authenticate();
    ${model} = {Model}::factory()->create();
    $this->delete(route('addon.{module}.delete', ${model}->id))->assertRedirect(route('addon.{module}.index'));

    $this->assertDatabaseCount('{module}', 0);
});