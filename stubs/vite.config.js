const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

import { defineConfig } from 'vite'
import path from "path"
import laravel from 'laravel-vite-plugin'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import IconsResolver from 'unplugin-icons/resolver'

const pathSrc = path.resolve(__dirname, "resources/assets/js/components")

export default defineConfig({
    build: {
        outDir: '../../public/addons-{module}',
        emptyOutDir: true,
        manifest: true,
        watch: {

        }
    },
    plugins: [
        laravel({
            publicDirectory: '../../public',
            buildDirectory: 'addons-{module}',
            input: [
                __dirname + '/resources/assets/sass/app.scss',
                __dirname + '/resources/assets/js/app.js'
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    // Vue 插件将重写资产 URLs，当被引用
                    // 在单文件组件中，指向 Laravel Web 服务
                    // 设置它为 `null` 允许 Laravel 插件
                    // 去替代重写资产 URLs 指向到 Vite 服务
                    base: null,

                    //  Vue 插件将解析绝对 URLs 
                    // 并把它们看做磁盘上的绝对路径。
                    // 设置它为 `false` 将保留绝对 URLs 
                    // 以便它们可以按照预期直接引用公共资产。
                    includeAbsolute: false,
                },
            },
        }),
        AutoImport({
                // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
                imports: ['vue'],
                resolvers: [
                    ElementPlusResolver(),
                    // 自动导入图标组件
                    IconsResolver({
                        prefix: 'Icon',
                    })
            ],
            // dts: true
            dts: path.resolve(__dirname, 'auto-imports.d.ts')
        }),

        Components({
            resolvers: [
                // 自动注册图标组件
                IconsResolver({
                enabledCollections: ['ep'],
                }),
                ElementPlusResolver(),
            ],
            // dts: true
            dts: path.resolve(__dirname, 'components.d.ts')
        }),
        Icons({
            autoInstall: true,
        }),
    ],
    resolve: {
        alias:{
            "@": pathSrc,
            "@js": path.resolve(__dirname, "resources/assets/js"),
            "@img": path.resolve(__dirname, "resources/assets/images"),
        }
    },
});
