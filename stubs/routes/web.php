<?php

use {namespace}\{Module}\Http\Controllers\{Module}Controller;


// Route::domain('admin.' . env('APP_DOMAIN'))->middleware('auth:sanctum')->prefix('addon/{module}')->group(function() {
Route::prefix('addon/{module}')->group(function() {
    Route::get('vue', [{Module}Controller::class, 'vue'])->name('addon.{module}.vue');
    Route::get('blade', [{Module}Controller::class, 'blade'])->name('addon.{module}.blade');
});

