import './bootstrap';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'


import { createApp } from "vue"
import App from "./App.vue"

import { createRouter, createWebHistory } from 'vue-router'

import { routes }  from '@js/routes'


const router = new createRouter({
    history: createWebHistory(),
    routes
})
const app = createApp(App)
app.use(router)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}



app.mount('#app')