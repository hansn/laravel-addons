let routes = [
    {
        path: '/addon/{module}/',
        name: 'Home',
        component: () => import('@/home/Home.vue'),
        children: [{
            path: 'vue',
            components: {
                default: () => import('@/home/DashboardMain.vue'),
            },
        },
        // {
        //     path: 'dashboard',
        //     components: {
        //         default: () => import('@/home/DashboardMain.vue'),
        //     },
        // },
    ],
        meta: {
            title: '{module} demo page'
        }
    }
]

export { routes }