# laravel-addons

> update at: 2022-12-20 04:23:00

## 介绍

该模块主要是为了方便laravel框架提供方便的插件管理功能

包括路由管理，artisan命令行管理

## 软件架构

laravel9 + vite + vue (Element plus)

## 安装教程

- 在项目根目录下运行：`composer require hansn/laravel-addons`
- 修改项目根目录下的`composer.json`如下所示

```json
   "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Addons\\": "addons/",
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/"
        }
    },
```

- 执行 `composer dump-autoload`

## 使用说明

- 创建插件模块`mymodule`：`php artisan addon:create mymodule`
- 执行插件模块`mymodule`的数据库迁移：`php artisan addon:migreate mymodule`
- 发布配置文件：`php artisan vendor:publish --provider="Hansn\LaravelAddons\Providers\BootstrapServiceProvider"`，会生成配置文件`config\addons.php`

> 默认的插件目录位于根目录下的`addons/`下的子目录


- 如果你不喜欢`addons`作为插件目录，可以修改配置文件`config\addons.php`里的配置项，如修改为`plugins`：

```php
    'path'=> 'plugins', //自定义的插件目录
```

，然后不要忘记修改根目录下的`composer.json`里的配置

```json
   "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Addons\\": "plugins/", //自定义的插件目录
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/"
        }
    },
```

最后再运行`composer dump-autoload`使其生效

## 定制你的模板文件

- 将`vendor\hansn\laravel-addons\src\Console\Commands\stubs`里的文件拷贝到`stubs/addons`下
- 修改配置文件`config/addons.php`里的配置项

```php
    'stubs'=>[
        'path'=> base_path('/stubs/addons'),
        // 'path'=> '',
    ],
```

- 对`stubs/addons`下的模板文件进行修改
- 执行创建`mymodule`插件模块命令 `php artisan addon:create mymodule`

## 启用/禁用插件


- 启用`mymodule`插件模块

```shell
php artisan addon:enable mymodule
```

- 禁用`mymodule`插件模块

```shell
php artisan addon:disable mymodule
```


> 插件模块的启用禁用是通过修改`addons/addons.json`文件来实现的

## 模块开发步骤

假定模块名为`mymodule`

### artisan命令

```shell
php artisan addon:create mymodule
php artisan addon:enable mymodule
php artisan addon:migrate mymodule
```

### 前端资源编译

```shell
cd addons/mymodule/
npm install
npm run dev

# 打包
npm run build
```

### 访问插件页面

- Blade默认页面访问

`http://127.0.0.1/addon/mymodule/blade`

- Element plus默认页面访问

`http://127.0.0.1/addon/mymodule/vue`

## 插件模块的路由

- api路由 `addons\mymodule\routes\api.php`
- web路由 `addons\mymodule\routes\web.php`

> 可通过 `php artisan route:list` 查看路由

## manifest.json

每个模块在创建时，都会生成新生成的模块目录下生成一个模块配置文件`manifest.json`，可用于对每个模块进行自定义的配置，开发者可根据自己需求自定义定义其中的字段，默认的配置说明如下：

```json
{
    "name":"模块名称",
    "module":"模块标识符",
    "desc":"描述信息",
    "html":"富文本描述信息",
    "image":"模块的图标",
    "author":"作者",
    "website":"模块的主页",
    "category":"模块类别",
    "order":"排序值",
    "tag":["标签1","标签2"],
    "version":"模块版本",
    "update_uri":"更新检测的地址"
}
```

> PS. 每个模块目录下面必须有`manifest.json`，但是里面的内容可以随意修改


## 定制化你的模板 stubs

### 模板标签

| 标识符 | 说明 |
| -- | -- |
| `{Module}` | 模块名驼峰写法 |
| `{module}` | 模块名全小写 |
| `{Model}` | 模型类名驼峰写法 |
| `{model}` | 模型类名全小写 |
| `{namespace}` | 命名空间 |
| `{addons_path}` | 插件目录名 |


### 1.发布stubs模板

```php
php artisan vendor:publish --provider="Hansn\LaravelAddons\Providers\BootstrapServiceProvider" --tag="stubs"
```

### 2.发布配置文件

```php
php artisan vendor:publish --provider="Hansn\LaravelAddons\Providers\BootstrapServiceProvider" --tag="config"
```

### 3.修改配置文件`config/addons.php`的stubs配置项

```php
    'stubs'=>[        
        '' => true,
        'path'=> base_path('/stubs/addons'),
    ],
```

### 4.修改`stubs/addons`下的模板

如修改 `stubs\Http\Controllers\ModuleController.php`

```php
<?php

namespace {namespace}\{Module}\Http\Controllers;

use Illuminate\Routing\Controller;

class {Module}Controller extends Controller
{
    // this is custom stub template
    public function index()
    {
        return view('{module}::home.index');
    }

}
```

### 6.生成插件模块或者生成控制器

```shell
# 生成mymodule插件模块
php artisan addon:create mymodule

# 为mymodule生成Demo控制器
php artisan addon:make-controller Demo mymodule
```
