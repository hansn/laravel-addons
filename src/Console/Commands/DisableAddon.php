<?php

namespace Hansn\LaravelAddons\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DisableAddon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addon:disable {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '禁用插件模块';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $module = ucfirst($this->argument('module'));
        $filePath = base_path(config('addons.path', 'addons'). DIRECTORY_SEPARATOR .'addons.json');
        $json = json_decode(file_get_contents($filePath));
        $json->$module = false;
        file_put_contents($filePath,json_encode($json,JSON_PRETTY_PRINT));
        // $this->info('模块【'.$module. '】禁用成功');

        return Command::SUCCESS;
    }
}
