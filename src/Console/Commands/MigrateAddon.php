<?php

namespace Hansn\LaravelAddons\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MigrateAddon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addon:migrate {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '插件数据库迁移';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $module = ucfirst($this->argument('module'));
        $path = config('addons.path', 'addons'). DIRECTORY_SEPARATOR ."${module}". DIRECTORY_SEPARATOR ."database". DIRECTORY_SEPARATOR ."migrations";
        echo '开始执行数据库迁移操作' . PHP_EOL;

        $this->call('migrate', [
            '--path' => $path,
        ]);

        echo '数据库迁移操作完成' . PHP_EOL;

        return Command::SUCCESS;
    }
}
