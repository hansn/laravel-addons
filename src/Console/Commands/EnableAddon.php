<?php

namespace Hansn\LaravelAddons\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class EnableAddon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addon:enable {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '启用模块';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $module = ucfirst($this->argument('module'));
        $filePath = base_path(config('addons.path', 'addons'). DIRECTORY_SEPARATOR .'addons.json');
        $json = json_decode(file_get_contents($filePath));
        $json->$module = true;
        file_put_contents($filePath,json_encode($json,JSON_PRETTY_PRINT));
        
        // $this->info('模块【'.$module. '】启用成功');

        return Command::SUCCESS;
    }
}
