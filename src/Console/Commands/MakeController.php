<?php

namespace Hansn\LaravelAddons\Console\Commands;

use Hansn\LaravelAddons\Services\AddonService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addon:make-controller {name} {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '为插件模块生成控制器，如生成Demo模块下的My目录下的Test控制器: php artisan addon:make-controller My\Test Demo';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(AddonService $addon)
    {
        $module = ucfirst($this->argument('module'));
        $name = ucfirst($this->argument('name'));
        //根据stubs生成目标文件
        $addon->convertStubByCmd('controller', $module, $name);
        return Command::SUCCESS;
    }
}
