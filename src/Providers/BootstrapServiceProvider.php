<?php

namespace Hansn\LaravelAddons\Providers;

use Hansn\LaravelAddons\Services\AddonService;
use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/addons.php' => config_path('addons.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../../stubs' => base_path('stubs/addons'),
        ], 'stubs');

    }

    /**
     * 注册应用服务
     *
     * @return void
     */
    public function register()
    {

        // Register a class in the service container
        $this->app->bind(AddonService::class, function ($app) {
            return new AddonService($app);
        });

    }
}
