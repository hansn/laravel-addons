<?php

namespace Hansn\LaravelAddons\Providers;

use Hansn\LaravelAddons\Console\Commands\{
    CreateAddon,
    DisableAddon,
    EnableAddon,
    MigrateAddon,
    MakeController
};

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAddon::class,
                MigrateAddon::class,
                DisableAddon::class,
                EnableAddon::class,
                MakeController::class,
            ]);
        }

    }
}
