<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Module Namespace
    |--------------------------------------------------------------------------
    |
    | Default module namespace.
    |
    */

    'namespace' => 'Addons',
    'path'=> 'addons', //插件模块目录

  
    'stubs'=>[        
        'enabled' => false,
        // 'path'=> base_path('stubs/addons'),
        'path'=> base_path('vendor/hansn/laravel-addons/stubs') ,
    ],

    'generate' => [ //每个生成命令对应的模块文件
        'controller' => 'Http/Controllers/ModuleController.php',
        'provider' => 'Providers/ModuleServiceProvider.php',
        'command' => 'Providers/ModuleServiceProvider.php',
    ]
];
